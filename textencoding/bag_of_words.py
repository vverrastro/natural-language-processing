# Implementiamo il modello Bag of words
# Usiamo Numpy

import numpy
import re

corpus = ["la mamma prepara la pasta", "la nonna prepara la pizza", "papà guarda la partita"]

def corpus_tokenizer(corpus):
    # in questo esempio non usiamo spacy e nltk 
    return [re.split("\W+", sent.lower()) for sent in corpus]

def build_vocab(corpus_tokens):
    vocab = set({})
    for tokens in corpus_tokens:
        for token in tokens:
            vocab.add(token)

    return list(vocab)

def bag_of_words(corpus, return_vocab=False):
    corpus_tokens = corpus_tokenizer(corpus)
    vocab = build_vocab(corpus_tokens)

    word_to_index = dict([word, i] for i, word in enumerate(vocab))

    docs_count = len(corpus)
    vocab_count = len(vocab)

    corpus_bow = numpy.zeros((docs_count, vocab_count))

    for i, tokens in enumerate(corpus_tokens):
        for token in tokens:
            corpus_bow[i][word_to_index[token]]+=1

    if(return_vocab):
        return corpus_bow, vocab
    else:
        return corpus_bow

corpus_bow, vocab = bag_of_words(corpus, return_vocab=True)
print(f"Vocabolario\n {vocab}")
print(f"Bag of words\n {corpus_bow}")

for sent, bow in zip(corpus, corpus_bow): # zip ci permette di eseguire un ciclo in simultanea
    print(f"Documento: {sent}")
    print(f"Bag of words: {bow}")