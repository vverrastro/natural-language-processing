# Natural Language Processing

These Python scripts are training for Natural Language Processing context.

## Installation

The scripts do not need installation.

## Usage

```gitbash
python stript_name.py
```

## Authors

- Vito Verrastro | verrastro.vito90@gmail.com
