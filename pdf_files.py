# Install PyPDF2 with pip

import PyPDF2

article_file = open("data/articolo.pdf", "ab+") # ab+ open in binary mode using b, because it is not a text file
article_pdf = PyPDF2.PdfFileReader(article_file)
type(article_pdf) # PdfFileReader

# Fix some special chars in the text
def fix_text(text):
    fixes = {'‘':'è','Õ':"'", 'Ò':'"', 'Ó':'"', '”':'è','Ÿ':'ò','‹':'à','š':'ù','!Ñ!':'-','ﬁ':'ì'}
    for char in fixes.keys():
        text = text.replace(char, fixes[char])
    return text

if (not article_pdf.getIsEncrypted()):
    print(article_pdf.numPages)

    page_one = article_pdf.getPage(0)
    type(page_one) # PageObject

    page_one_text = page_one.extractText()
    print(page_one_text)
    print(fix_text(page_one_text))

text = ""

# Read all pdf pages and prepare the text
for i in range(article_pdf.numPages):
    page_text = fix_text(article_pdf.getPage(i).extractText())
    text+=page_text + "\n"

print(f"Total letters in the document: {len(text)}")
print(f"Total words in the document: {len(text.split())}")

text = text.replace("\n", "")
text = text.replace(".", ".\n")

print(text)