# pip install tqdm - progress bar

from sklearn.utils import shuffle
from sklearn.feature_extraction.text import CountVectorizer

import numpy as np
from os import listdir
from tqdm import tqdm

files_path = "./data/aclImdb/"

def load_xy(path, labels=["pos", "neg"]):

    reviews = []
    y = []

    labels_map = { "pos": 1, "neg": 0}

    for label in labels:
        path_file = path + "/" + label
        print("File path: " + path_file)
        for file_name in tqdm(listdir(path_file)):
            review_file = open(path_file + "/" + file_name, encoding="utf-8")
            review = review_file.read()
            review_file.close()

            reviews.append(review)
            y.append(labels_map[label])
    
    return (reviews, y)

print("\n\n")

# PREPROCESSING

reviews_train, y_train = load_xy(files_path + "train")
reviews_test, y_test = load_xy(files_path + "test")

bow = CountVectorizer(max_features=5000) # bag_of_words, numero massimo di parole da prendere in considerazione tra quelle più comuni

# Trasformatori (per trasformare i dati) ed Estimatori (per costruire i modelli)[fit (calcoli), transform]
# Fit and Transform

bow_train = bow.fit_transform(reviews_train) # Matrice sparsa
bow_test = bow.transform(reviews_test) # Usiamo gli stessi calcoli usati per il test, perciò usiamo solo transform

X_train = bow_train.toarray()
X_test = bow_test.toarray()

# Standardizzare i dati

from sklearn.preprocessing import StandardScaler
ss = StandardScaler()

X_train = ss.fit_transform(X_train)
X_test = ss.transform(X_test)

print("\n\nTRAIN\n")
print(X_train.mean())
print(X_train.std())
print("\n\nTEST\n")
print(X_test.mean())
print(X_test.std())

# REGRESSIONE LOGISTICA
print("\n\n")

from sklearn.linear_model import LogisticRegression

lr = LogisticRegression(C=0.001) # Parametro di regolarizzazione (penalizziamo i coefficienti eccessivamente grandi) per evitare l'overfitting "memorizza i dati di training anzichè apprendere"
lr = lr.fit(X_train, y_train)

from sklearn.metrics import log_loss, accuracy_score # Funzione di costo, percentuale di classificazione

train_pred = lr.predict(X_train)
train_pred_proba = lr.predict_proba(X_train)

print(accuracy_score(y_train, train_pred))
print(log_loss(y_train, train_pred_proba))

test_pred = lr.predict(X_test)
test_pred_proba = lr.predict_proba(X_test)

print(accuracy_score(y_test, test_pred))
print(log_loss(y_test, test_pred_proba))

print("\n\n")
review = "This is the best movie I've ever seen"
x = bow.transform([review])
print(lr.predict(x)) # POSITIVA
review = "This is the worst movie I've ever seen"
x = bow.transform([review])
print(lr.predict(x)) # NEGATIVA