import nltk
from nltk.corpus import stopwords
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

nltk.download("stopwords")
nltk.download("punkt")

import string
import numpy as np
from os import listdir
from tqdm import tqdm

files_path = "./data/aclImdb/"

def get_dataset(path, labels=["pos", "neg"], sample_per_class=1000):

    dataset = []

    for label in labels:
        count = 0
        path_file = path + "/" + label
        print("File path: " + path_file)
        for file_name in tqdm(listdir(path_file)):
            review_file = open(path_file + "/" + file_name, encoding="utf-8")
            review = review_file.read()
            review = review.translate(str.maketrans("", "", string.punctuation)) # Sostituiamo i caratteri di punteggiatura con quelli vuoti
            review = review.lower()

            words = nltk.word_tokenize(review)
            # words_filtered = [word for word in words if word not in stopwords.words("english")]
            # words_dict = dict([(word, True) for word in words_filtered])

            words_dict = dict([word, True] for word in words if word not in stopwords.words("english"))
            dataset.append((words_dict, label))

            count += 1
            review_file.close()

            if(sample_per_class!=None):
                if(count >= sample_per_class):
                    break

    return dataset

print("\n\n")

train_set = get_dataset(files_path + "train", sample_per_class=1000)
test_set = get_dataset(files_path + "test", sample_per_class=1000)
print(len(train_set))

# CLASSIFICAZIONE

from nltk.classify import NaiveBayesClassifier

classifier = NaiveBayesClassifier.train(train_set)

from nltk.classify.util import accuracy

print(accuracy(classifier, train_set))
print(accuracy(classifier, test_set))

classifier.show_most_informative_features()

def create_word_features(review):
    review = review.translate(str.maketrans("", "", string.punctuation)) # Sostituiamo i caratteri di punteggiatura con quelli vuoti
    review = review.lower()
    words = nltk.word_tokenize(review)

    words_dict = dict([word, True] for word in words if word not in stopwords.words("english"))

    return words_dict

review = "This movie was just great"
x = create_word_features(review)
print(classifier.classify(x))
review = "This movie was just terrible"
x = create_word_features(review)
print(classifier.classify(x))