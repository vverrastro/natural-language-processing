# POS - Part of speech
# Processo di identificatione della parte del discorso di ogni parola di un testo

# Metodi Lessicali (testo già etichettato) assegnamo il POS più frequente per una determinata parola

# Metodi basati su regole (parole che finiscono con "are/avo" sono verbi, etc..)

# Metodi probabilistici 

# Deep learning (Reti neurali ricorrenti)

# Il POS tagging è un'operazione di basso livello utilizzata per operazioni di più alto livello
# Utile per la lemmatizzazione, tokenizzazione, Named Entity Recognition

import nltk
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

nltk.download("averaged_perceptron_tagger")

text = "Every day is a new opportunity to change your life"

tokens = nltk.word_tokenize(text)
tags = nltk.pos_tag(tokens) # Supporta solo la lingua inglese, per l'italiano usare spacy

# Per accedere alla descrizione dei tags
nltk.download("tagsets")
nltk.help.upenn_tagset()

print(text)
print(tags)

text = "Machine learning is the study of computer algorithms that improve automatically through experience. It is seen as a subset of artificial intelligence."
tokens = nltk.word_tokenize(text)
tags = nltk.pos_tag(tokens)

print(tags)

tokens_tag = [token + "(" + tag + ")" for token, tag in tags]
print(tokens_tag)

tokens_tag = " ".join(tokens_tag)
print(tokens_tag)