# Dalla forma flessa alla forma canonica (lemma=parola reale)
# Stesso scopo dello stemming ma effettuando un'analisi morfologica
# Non usa un set di regole prefissato
# Porta a migliori risultati perchè usica tecniche intelligenti considerando il contesto

import nltk
from nltk.stem import WordNetLemmatizer
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

nltk.download("wordnet")

text_en = "My cats ate some mice while I was playing King of the Riddles"
text_it = "I miei gatti hanno mangiato un topo mentre io stavo giocando al computer"

tokens_en = nltk.word_tokenize(text_en, "english")

lemmatizer = WordNetLemmatizer()

token_lem_en = [lemmatizer.lemmatize(token) for token in tokens_en] # Considera anche le maiuscole / minuscole (e.i. nomi propri)

print(token_lem_en)


# Come vedi a lemmatizzato perfettamente i nomi (mice -> mouse), però non ha fatto altrettanto con i verbi, perchè ? 
# Perché dobbiamo anche specificare il tipo di parola utilizzando il parametro pos, se non lo facciamo crederà che si tratti di un nome.

tokens = [("My","n"),("cats","n"),("ate","v"), ("some","n"), ("mice","n"), ("while","r"), ("I","n"), ("was","v"), ("playing","v"), ("King","v"), ("of","n"),("the","n"),("Riddles","n")]

token_lem_en = [lemmatizer.lemmatize(token[0], pos=token[1]) for token in tokens]

print(token_lem_en)
