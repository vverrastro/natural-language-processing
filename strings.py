# Operations with strings

my_string = "This is my string!"
print(my_string)
print(my_string[1])
print(my_string[-1])
print(my_string[2:3])
print(my_string[:-1])
print(type(my_string))
print("----------------------------")

# The strings are immutable but we can reassign a value

my_string = "t" + my_string[1:]
print(my_string)
print(my_string.lower())
print(my_string.upper())
print(my_string.capitalize())
print("----------------------------")

# Split - Used to create a string list from a string

my_string = "Questa è una frase di prova"
my_list = my_string.split() # I create a list of words from a string
print(my_list)
print(" ".join(my_list)) # I create a string from a list of words
my_string = "Oggi è una bella giornata. Andrò a fare una corsa al parco. Per cena mangerò della carne"
sentences = my_string.split(".")
print(sentences)
print("----------------------------")

# Count - Used to count how many times a string is present in another

print(my_string.count("è"))
print(my_string.count("Ananas"))
print("----------------------------")

# Replace - Used to replace a string

my_string = my_string.replace("Andrò", "E poi andrò")
print(my_string)
print("----------------------------")

# Find - Used to find the index of a character (find, rfind)

my_string = "Oggi è una bella giornata. Andrò a fare una corsa al parco. Per cena mangerò della carne"
start = my_string.find("Andrò")
substring = my_string[start:]
print(start)
print(substring)
print(my_string.find("Giovanni"))
my_string = "Occhio per occhio dente per dente"
print(my_string.find("dente"))
print(my_string.rfind("dente"))
print("----------------------------")

# Strip - Used to remove spaces before and after the string (strip, lstrip, rstrip)

my_string = "   lascio degli spazi vuoti  "
print(my_string.strip())
print(my_string.lstrip()) # left
print(my_string.rstrip()) # right
print("----------------------------")