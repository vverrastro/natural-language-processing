# NLTK - Natural Language Toolkit

import nltk
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

# nltk.download("all") # All models and corpus

nltk.download("punkt") # tokenizer

text = "Cos'è questa fretta? Facciamolo un'altra volta, ti va bene?"

tokens_split = text.split()
print(tokens_split)

from nltk import word_tokenize # IT was not supported, but it seems to work correctly

tokens = word_tokenize(text)
print(tokens)

text = "I'd like to visit U.S.A., in particular San Francisco but I can't afford it"

tokens = word_tokenize(text)
print(tokens)

# How tokenize utterances in a corpus

from nltk.tokenize import sent_tokenize

text = ("I'd like to visit U.S.A., in particular San Francisco but I can't afford it."
        " Indead I'll visit U.K this summer.")

tokens = sent_tokenize(text)
print(tokens)

# Remove stop words

from nltk.corpus import stopwords

nltk.download("stopwords")

en_stopwords = stopwords.words("english")
print(len(en_stopwords))
print(en_stopwords[:10])

it_stopwords = stopwords.words("italian")
print(len(it_stopwords))
print(it_stopwords[:10])

text = "Io sono una persona simpatica, solo che non mi piacciono gli essere umani, preferisco i gatti."

tokens = word_tokenize(text, "italian")
print(tokens)
tokens_filtered = []
tokens_stopword = []

for token in tokens:
        if (token.lower() not in it_stopwords):
                tokens_filtered.append(token)
        else:
                tokens_stopword.append(token)

print(tokens_filtered)
print(tokens_stopword)

# We can create the same arrays using List Comprehension x = [expression for i in list condition]

tokens_filtered = [token for token in tokens if token.lower() not in it_stopwords]
tokens_stopword = [token for token in tokens if token.lower() in it_stopwords]

print(tokens_filtered)
print(tokens_stopword)