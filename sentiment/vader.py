# Valene Aware Dictionary for Sentiment Reasoning
# Usato spesso sui social media, contiene un dizionare di parole già annotate [positive e negative]

# Dizionario più regole lessicali

def sentiment_to_label(sentiment_value):
    
    if(sentiment_value <= -0.75):
      return "fortemente negativa"
    elif(sentiment_value <= -0.5):
      return "molto negativa"
    elif(sentiment_value <= -0.25):
      return "piuttosto negativa"
    elif(sentiment_value <=0):
      return "negativa"
    elif(sentiment_value<=0.25):
      return "piuttosto positiva"
    elif(sentiment_value<=0.5):
      return "positiva"
    elif(sentiment_value<=0.75):
      return "molto positiva"
    elif(sentiment_value>0.75):
      return "fortemente positiva"
    
    return "errore"

import nltk
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
nltk.download("vader_lexicon")
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SentimentAnalyzer

sentiment_analyzer = SentimentAnalyzer()

text = "I love this app"
print(sentiment_analyzer.polarity_scores(text)) # compound da -1 a 1, recensione positivia

text = "I didn't like this app, unistalled"
print(sentiment_analyzer.polarity_scores(text)) # recensione negativa

text = "I LOVE this app" # maiuscole e punteggiature influenzano il sentiment
print(sentiment_analyzer.polarity_scores(text)) # recensione negativa

print("\n\n-----------------------------\n\n")

import pandas as pd

reviews = pd.read_csv("./data/recensioni_sentiment.csv", sep=";")
print(reviews.head())
print("\n\n")

full_reviews = reviews["title"] + " " + reviews["review"]
print(full_reviews.head())
print("\n\n")

print(full_reviews.loc[0])
print("\n\n")
print(sentiment_analyzer.polarity_scores(full_reviews.loc[0]))

reviews["sentiment score"] = full_reviews.apply(lambda review: sentiment_analyzer.polarity_scores(review)["compound"])
reviews["sentiment label"] = reviews["sentiment score"].apply(lambda sentiment: sentiment_to_label(sentiment))
print(reviews.head())

# Anche le recensioni negative sono state elaborate positivamente perchè è un modello semplice