from bs4 import BeautifulSoup
from urllib.request import urlopen
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

html_page = urlopen("https://profession.ai")
html_code = html_page.read()

soup = BeautifulSoup(html_code, "lxml")
print(soup.prettify()) # We can see the html code 

paragraphs = soup.find_all("p")
text = ""

for p in paragraphs:
    text += p.get_text() + " "

text = " ".join(text.split()).replace(".", ".\n")
print(text)