# Dividere la radice di una parola dalla desinenza "parte variabile"
# Portare le parole nella loro forma base

from nltk.stem.porter import PorterStemmer
from nltk import word_tokenize

# PORTER STEMMER

text_en = "My cats ate some mice while I was playing King of the Riddles"
text_it = "I miei gatti hanno mangiato un topo mentre io stavo giocando al computer"

tokens_en = word_tokenize(text_en, "english")
tokens_it = word_tokenize(text_it, "italian")


stemmer = PorterStemmer()
token_stem_en = [stemmer.stem(token) for token in tokens_en]
token_stem_it = [stemmer.stem(token) for token in tokens_it] # Non funziona in italiano

print(tokens_en)
print(token_stem_en)

print(tokens_it)
print(token_stem_it)

# SNOWBALL STEMMER - L'italiano è supportato (solitamente risultati migliori)

from nltk.stem.snowball import SnowballStemmer

text_en = "My cats ate some mice while I was playing King of the Riddles"
text_it = "I miei gatti hanno mangiato un topo mentre io stavo giocando al computer"

tokens_en = word_tokenize(text_en, "english")
tokens_it = word_tokenize(text_it, "italian")

stemmer = SnowballStemmer("english")
token_stem_en = [stemmer.stem(token) for token in tokens_en]
stemmer = SnowballStemmer("italian")
token_stem_it = [stemmer.stem(token) for token in tokens_it] # Non funziona in italiano

print(tokens_en)
print(token_stem_en)

print(tokens_it)
print(token_stem_it)

# LANCASTER STEMMER - Regole aggressive

from nltk.stem.lancaster import LancasterStemmer

text_en = "My cats ate some mice while I was playing King of the Riddles"
text_it = "I miei gatti hanno mangiato un topo mentre io stavo giocando al computer"

tokens_en = word_tokenize(text_en, "english")
tokens_it = word_tokenize(text_it, "italian")

stemmer = LancasterStemmer()
token_stem_en = [stemmer.stem(token) for token in tokens_en]

print(tokens_en)
print(token_stem_en)