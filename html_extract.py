from bs4 import BeautifulSoup

html_file = open("data/article.html", encoding="utf-8")
html_code = html_file.read()

# We pass here the html code to BS class
article = BeautifulSoup(html_code, "lxml")

print(article.prettify())
print(article.get_text())

body = article.body # Type Tag
title = article.title.get_text() # Get page title
first_p = article.p # Get first p

all_paragraphs = article.find_all("p") # ResultSet
print(all_paragraphs[0].get_text())

text = ""

for p in all_paragraphs:
    text += p.get_text()

text = " ".join(text.split()).replace(".", ".\n")

print(text)
print(f"Il file contiente {len(text)} caratteri.")
print(f"Il file contiente {len(text.split())} parole.")