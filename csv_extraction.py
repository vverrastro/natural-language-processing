import pandas as pd

# Path to the local csv file or url to remote 
reviews = pd.read_csv("data/reviews.csv") # DataFrame type

# First 5 lines
print(reviews.head()) # head(10) to change the number

# Info about the file, columns number and rows
print(reviews.info())

reviews_text = reviews["Review Text"] # Type Series
reviews_text = reviews_text.dropna()
print(reviews_text)

# To open a specific item with ID
print(reviews_text.loc[4]) # Identifier
print(reviews_text.iloc[-1]) # With Index

new_reviews = pd.concat([reviews_text, reviews["Star Rating"]], axis=1) # Use 1 to concat by column and 0 for row
new_reviews = new_reviews.dropna()

reviews_len = reviews_text.map(len) # Type Series

new_reviews["Review Length"] = reviews_len
print(new_reviews.mean())
print(new_reviews.describe())
print(new_reviews.head())