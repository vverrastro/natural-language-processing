# NER è il processo di identificazione della classe di appartenenza di una parola all'interno di un documento di testo
# Classe di appartenenza: macro categoria (ES. Elon Musk = Persona, Google = Organizzazione, Roma = Luogo)

# nltk non ha un metodo implementato per eseguire il NER

import spacy

nlp = spacy.load("en_core_web_sm")

text = "Mark Zuckerberg acquired Whatsapp for 15 billions USD on 15 August 2014."

doc = nlp(text)

print(doc.ents) # Spacy già esegue l'operazione di NER
print(doc.ents[0])
print(doc.ents[0].label_)
print(spacy.explain(doc.ents[0].label_))

print("\nENTITY\t\tTAG\t\tDESCRIZIONE")

for ent in doc.ents:
    print(ent.text + "\t\t" + ent.label_ + "\t\t" + spacy.explain(ent.label_))

text_ents = text

for i in range(0, len(doc.ents)):
    text_ents = text_ents.replace(doc.ents[i].text, doc.ents[i].text + "(" + doc.ents[i].label_ + ")")

print(text_ents)
print("\n\n-----------------------\n\n")

nlp = spacy.load("it_core_news_sm")

text = ("Il 15 Gennaio 2021 Pippo Baudo ha venduto la sua azienda NIGMA a Mediaset per 30 milioni di euro ed è andato a vivere a Tenerife in Spagna"
        " con il suo gatto Micio.")

doc = nlp(text)
print(doc.ents) 

text_ents = text

for i in range(0, len(doc.ents)):
    text_ents = text_ents.replace(doc.ents[i].text, doc.ents[i].text + "(" + doc.ents[i].label_ + ")")

print(text_ents)

# Correzione delle entità
from spacy.tokens import Span

money_tag = doc.vocab.strings["MONEY"]
date_tag = doc.vocab.strings["DATE"]

money = Span(doc, 15, 19, label=money_tag) # index di partenza più quello di fine + 1
date = Span(doc, 1, 4, label=date_tag)

doc.ents = list(doc.ents)+[money]
ents = list(doc.ents)
del ents[0]
doc.ents = ents+[date]

print(doc.ents)

print("\nENTITY\t\tTAG\t\tDESCRIZIONE")

for ent in doc.ents:
    print(ent.text + "\t\t" + ent.label_ + "\t\t" + spacy.explain(ent.label_))

text_ents = text

for i in range(0, len(doc.ents)):
    text_ents = text_ents.replace(doc.ents[i].text, doc.ents[i].text + "(" + doc.ents[i].label_ + ")")

print(text_ents)
print("\n\n-----------------------\n\n")

# Per una visualizzazione diversa

from spacy import displacy

print(displacy.render(doc, style="ent"))