import spacy
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

text = "Every day is a new opportunity to change your life"

nlp = spacy.load("en_core_web_sm")

doc = nlp(text)

print(doc[0].text)
print(doc[0].tag_)
print(doc[0].pos_)

print(spacy.explain(doc[0].tag_))


print("\n\nTOKEN\t\tPOS\t\tTAG\t\tDESCRIZIONE\n")
for token in doc:
    print(token.text + "\t\t" + token.pos_ + "\t\t" + token.tag_ + "\t\t" + spacy.explain(token.tag_))


nlp = spacy.load("it_core_news_sm")

text = "Ottima app, su certi enigmi ci si deve ragionare un po', ma d'altronde non è un gioco bello un gioco che finisce in un giorno"

doc = nlp(text)

print("\n\nTOKEN\t\tPOS\t\tTAG\t\tDESCRIZIONE\n")
for token in doc:
    print(token.text + "\t\t" + token.pos_ + "\t\t" + token.tag_ + "\t\t NON DISPONIBILE")