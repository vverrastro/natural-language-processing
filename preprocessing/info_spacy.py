# Libreria NLP ad alte prestazioni (meno metodi rispetto a nltk, ma eccellenti)
# Non implementa tecniche per lo stemming, solo lemming
# Supporta anche l'italiano

import spacy

#python -m spacy download en_core_web_sm

nlp = spacy.load("en_core_web_sm")

text = "That's very kind of you, brothers, but I saw you so lost."

doc = nlp(text) # Qui vengono applicati tutti i metodi

for token in doc:
    print(token.text)

for sent in doc.sents: # Non è una lista, non posso accedere ad un elemento usando un indice
    print(sent.text)

sent_list = list(doc.sents)
print(sent_list[0].text)

print("\n\nTOKEN\t\tLEMMA")

for token in doc:
    print(f"{token.text}\t\t{token.lemma_}") # IL _ è importante, altrimenti vediamo gli HASH di riferimento per i LEMMA

stopwords = nlp.Defaults.stop_words
print(len(stopwords))
print(list(stopwords)[:10])

# Possiamo controllare per ogni token se si tratta di una stopword usando la property is_stop

print("\n\nTOKEN\t\tIS STOP")

tokens_filtered = [token for token in doc if not token.is_stop] # Ho creato una nuova lista senza stop words | Oggetti di tipo Token | lista di stringhe token.text 

for token in doc:
    print(f"{token.text}\t\t{token.is_stop}") 

print(tokens_filtered)

print("\n\n-------- IT ---------\n\n")

# python -m spacy download it_core_news_sm

nlp = spacy.load("it_core_news_sm")

doc = nlp("Oggi è una giornata afosa. Io ho davvero voglia di una granita fresca.")

print("PROPOSIZIONI")
print([sent for sent in doc.sents])
print("TOKEN\t\tLEMMA\t\tIS STOP")
for token in doc:
    print(f"{token.text}\t\t{token.lemma_}\t\t{token.is_stop}")