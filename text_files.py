proverbs_file = open("data/proverbi.txt", encoding="utf-8")
print(type(proverbs_file))

print(proverbs_file.read())
print(proverbs_file.seek(0)) # Reset the index
print(proverbs_file.readlines()) # Rows arrays

print(proverbs_file.seek(0)) # Reset the index

for i, proverb in enumerate(proverbs_file.readlines()):
    print("Proverb %d: %s" % (i + 1, proverb[:-1]))

# In a FOR cicle we can drop the function readlines
for i, proverb in enumerate(proverbs_file):
    print("Proverb %d: %s" % (i + 1, proverb[:-1]))

# It necessary to close the file after opening
proverbs_file.close()

# Or use a context 

with open("data/proverbi.txt", encoding="utf-8") as proverbs_file:
    proverbs = proverbs_file.readlines()

print(proverbs)

# Write a file
# Permission
# r: only read
# r+: write and read permission, the curser is set at the start of the file
# w: only read
# w+: write and read permission, if the file exists it's overwrited else it creats a new one
# a: write permission, without cancel the current file body, cursor at the end of the file
# a+: write and read permission, if the file exists it's opened without overwriting else it creats a new one


# Only write
proverbs_file = open("data/proverbi.txt", "a", encoding="utf-8")

new_proverb = "\nRosso di sera bel tempo si spera"

proverbs_file.write(new_proverb)
proverbs_file.close()

# Read and write but the curser is at the end of the file
proverbs_file = open("data/proverbi.txt", "a+", encoding="utf-8")

proverbs_file.seek(0)
proverbs_file.readlines()

new_proverb = "\nRose rosse rose blu"
proverbs_file.write(new_proverb)
proverbs_file.close()

# Create a new file with first 3 proverbs
proverbs_file = open("data/proverbi.txt", "r+", encoding="utf-8")
new_proverbs_file = open("data/proverbi_new.txt", "w+", encoding="utf-8")

for i, proverb in enumerate(proverbs_file):
    if(i >= 3):
        break
    print(proverb)
    new_proverbs_file.write(proverb)

new_proverbs_file.seek(0)
print(new_proverbs_file.readlines())

proverbs_file.close()
new_proverbs_file.close()