from docx import Document

document = Document("data/articolo_ml.docx")

first_paragraph = document.paragraphs[0]
print(first_paragraph.text)

text = ""

for paragraph in document.paragraphs:
    text += paragraph.text

text = text.replace(".", ".\n")
text = text.replace("?", "?\n")

print(text)
print(f"Il file contiente {len(text)} caratteri.")
print(f"Il file contiente {len(text.split())} parole.")