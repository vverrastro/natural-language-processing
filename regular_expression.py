import re

text = "Questa corso di Natural Language Programming spacca!"
pattern = "corso"

search_pattern = re.search(pattern, text)
print(search_pattern)
print(search_pattern.span()) # Index start and end .start() . end()

print(text[search_pattern.start():search_pattern.end()])
search_pattern.group() # corso

text = ("Questa corso di Natural Language Programming spacca, è il migliore di sempre"
        "e per qualsiasi problema posso chiamare Alessandro al +39 333-123-9876"
        "(non è un numero vero)! Oppure al numero +44 777-434-3333") # () to write strings on more rows


pattern = r"(\d{2}) (\d{3})-(\d{3}-\d{4})" # \d\d\d \d\d\d \d\d\d\d
search_pattern = re.search(pattern, text)

print(search_pattern.group(1))
print(search_pattern.group(2))
print(search_pattern.group(3))

text = ("Questa corso di Natural Language Programming spacca, è il migliore di sempre"
        "e per qualsiasi problema posso chiamare Alessandro al +39 333-123-9876"
        "(non è un numero vero)! Oppure al numero +44 777-4334-333") # () to write strings on more rows

pattern = r"(\d{2}) (\d{3})-(\d{3,5}-\d{3,5})" # \d\d\d \d\d\d \d\d\d\d

search_pattern = re.findall(pattern, text)

print(search_pattern)
print(search_pattern[0][1])

text = ("Per assistenza scrivi pure a supporto@test.ai oppure"
       "oppure prova mini@testdue.it")

pattern = r"[a-z0-9]+@[a-z]+\.[a-z]{2,3}" # . all char except new line, we use excape

search_pattern = re.findall(pattern, text)
print(search_pattern)

text = ("Per assistenza scrivi pure a supporto@test.ai oppure"
       "oppure prova mini@testdue.it")

pattern = r"\S+@\S+" # S all non spaces | s all spaces

search_pattern = re.findall(pattern, text)
print(search_pattern)

text = ("Questa corso di Natural Language Programming spacca, è il miglior corso di sempre"
        "(o almeno spero che lo sia...)")

# pattern = r"[!.,]"
pattern = r"[^\w\s]" # ^ is NOT, \w chars and numbers, \s spaces

text = re.sub(pattern, ' ', text) # substitution 
print(text)

text = ("   Questa      corso di Natural     Language Programming spacca,     è il miglior corso di    sempre"
        "(o almeno spero che lo sia...)")

pattern = r" +" # remove more spaces
text = re.sub(pattern, ' ', text)
print(text)