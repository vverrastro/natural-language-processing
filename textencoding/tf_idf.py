# TF+IDF - Term Frequency * Inverse Document Frequency
# Modello per la rappresentazione delle parole
# Da peso alle più rare e meno peso a quelle più comuni

# IDF - log (NDocumenti / DocumentFrequency)
# TF - NTermInDocument / NTotalTermInDocument

# Moltiplico alla fine TF*IDF 

import numpy as np
import re

corpus = ["la mamma, prepara la pasta", "la nonna prepara, la pizza", "papà! guarda la partita"]

def corpus_tokenizer(corpus):
    return [re.split("\W+", sent.lower()) for sent in corpus]

def build_vocab(corpus_tokens):
    vocab = set({})
    for tokens in corpus_tokens:
        for token in tokens:
            vocab.add(token)

    return list(vocab)

def tf_idf(corpus, return_vocab=False):

    corpus_tokens = corpus_tokenizer(corpus)
    index_to_word = build_vocab(corpus_tokens)

    word_to_index = dict([word, i] for i, word in enumerate(index_to_word))

    docs_count = len(corpus)
    vocab_size = len(index_to_word)

    # Document Frequency - In quanti documenti compare un termine

    df = np.zeros(vocab_size)

    for i, word in enumerate(index_to_word):
        for tokens in corpus_tokens:
            if(word in tokens):
                df[i]+=1

    # Inverse Document Frequency - Dare peso alle parole più rare

    idf = np.log(docs_count/df) + 1

    # Term Frequency - Quante volte un termine compare in un documento

    tf = np.zeros((docs_count, vocab_size))

    for i, tokens in enumerate(corpus_tokens):
        word_counts = len(tokens)
        for token in tokens:
            tf[i][word_to_index[token]]+=1
        tf[i]/=word_counts

    tf_idf = tf*idf

    print(f"TF*IDF \n{tf_idf}")

    if(return_vocab):
        return tf_idf, index_to_word
    else:
        return tf_idf

corpus_tf_idf, vocab = tf_idf(corpus, return_vocab=True)

print(f"Vocabolario: \n {vocab}")

for sent, tfidf in zip(corpus, corpus_tf_idf):
    print(f"Documento: \n{sent}")
    print(f"TF*IDF: \n{tfidf}")
    print("----------------------")